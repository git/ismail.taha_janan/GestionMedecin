/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package gestionmedecin;

/**
 *
 * @author istahajana
 */
public class GestionMedecin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Afficheur monAfficheur = new Afficheur();
        Lecteur monLecteur = new Lecteur();
        PatientList todayList = new  PatientList();
        
        
        boolean loop = true;
        
        
        
        while (loop) {

            monAfficheur.menu();
            
            String var = monLecteur.lireMenu();
            
            switch (var) {
                case "1" -> {
                    Patient x =  monLecteur.addNewPatient();
                    todayList.addPatient(x);
                }
                case "2" -> monAfficheur.afficherList(todayList);
                case "3" -> loop=false;
                default -> throw new AssertionError();
            }


        }
        
    }
    
}
