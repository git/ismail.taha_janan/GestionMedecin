/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package gestionmedecin;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author istahajana
 */
public class PatientList {
    List<Patient> malist = new ArrayList<>();
    
    public void addPatient(Patient passion){
        malist.add(passion);
    }
    
    public List<Patient> getList(){
        return malist;
    }
}
